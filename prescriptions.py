import time
import numpy as np
import matplotlib.pyplot as plt

import os
from sklearn import svm
import shutil
from tqdm import tqdm
import pandas as pd
import redis
from bokeh.plotting import figure, output_file, show, output_notebook
from bokeh.models import ColumnDataSource
from enum import Enum

output_notebook()

def get_redis_details():
    with open('../Shared/redis', 'r') as f:
        host, password = [s.strip() for s in f.readlines()]
    return host, password
    
def get_my_month_year():
    user = os.getenv('JUPYTERHUB_USER')
    num = int(user.split('-')[-1])
    month, year = (num - 1) % 12 + 1, 2015 + (num - 1) // 12
    return month, year

def fetch_prescription_data(filename):
    shutil.copyfile('../Shared/prescriptions1317.csv', filename)
    
def load_all_prescription_data(filename):
    chunksize = 10 ** 5
    with tqdm(total=21872313) as pbar:
        df = pd.DataFrame()
        for chunk in pd.read_csv(filename, chunksize=chunksize, low_memory=False):
            df = pd.concat([df, chunk])
            pbar.update(chunksize)
    return df

def load_prescription_data_filter_by_month(filename, month, year):
    chunksize = 10 ** 5
    with tqdm(total=21872313) as pbar:
        df = pd.DataFrame()
        for chunk in pd.read_csv(filename, chunksize=chunksize, low_memory=False):
            df = pd.concat([df, chunk[(chunk['Month'] == month) & (chunk['Year'] == year)]])
            pbar.update(chunksize)
    return df

def get_breakdown_by_prescription_cost(df):
    bins = [0, 1, 10, 100, 1e6]
    cut = pd.cut(df['Actual Cost (£)'], bins)
    breakdown = df.groupby(cut).count()['Total Items']
    print(breakdown.sum(), df['Total Items'].count())
    return breakdown

def send_one_month_to_datastore(month, year, breakdown):
    redis_server, redis_pass = get_redis_details()
    r = redis.Redis(host=redis_server, password=redis_pass)

    for group, val in breakdown.items():
        print(group, val)
        r.hset(f"{year}-{month}", str(group), val)
        
def fetch_breakdowns_from_datastore():
    redis_server, redis_pass = get_redis_details()
    r = redis.Redis(host=redis_server, password=redis_pass)
    all_months = []
    for num in range(25):
        month, year = (num - 1) % 12 + 1, 2015 + (num - 1) // 12
        groups = r.hgetall(f"{year}-{month}")
        groups = {key.decode('utf-8'): int(val) for key, val in groups.items()}
        all_months.append([(year, month), groups])
    over_time = pd.DataFrame({ym: groups for ym, groups in all_months}).transpose()
    return over_time

def plot_breakdowns_together(over_time):
    p = figure(x_axis_type='datetime')
    over_time['Date'] = [f"{y}-{m}" for y, m in over_time.index]
    over_time['Date'] = pd.to_datetime(over_time['Date'], format='%Y-%m')
    over_time = over_time.dropna()
    source = ColumnDataSource(over_time)
    p.line(x='Date', y='(0.0, 1.0]', color='red', line_width=2, source=source, legend_label='Under £1')
    p.line(x='Date', y='(1.0, 10.0]', color='blue', line_width=2, source=source, legend_label='£1 - £10')
    p.line(x='Date', y='(10.0, 100.0]', color='green', line_width=2, source=source, legend_label='£10 - £100')
    p.line(x='Date', y='(100.0, 1000000.0]', color='black', line_width=2, source=source, legend_label='>£100')

    show(p)
    
class Presentation(Enum):
    TABLET = 1
    CAPSULE = 2
    DROP = 3
    OTHER = 4
    
def add_known_pres(df):
    df['Known Pres'] = df.apply(clean_presentation, axis=1)
    df = df[df['Known Pres'] != Presentation.OTHER.value]
    return df

def count_presentation_by_suffix(df, suffix):
    return pd.DataFrame({f"-{suffix}": {
        Presentation(p).name: v
        for p, v in
        df[df['suffix'] == suffix].groupby('Known Pres').count()['Total Items'].items()
    }}).sort_index(inplace=False)

# Many nicer ways, but succinct and quick
def suffix_map(suff):
    return sum([o for n, o in enumerate(ord(c) - ord('a') for c in suff)]) / (26 * 4)

def dataframe_to_features(df):
    denorm_len = df['VTM_NM'].str.len().max()
    denorm_cost = df['Actual Cost (£)'].max()
    features = pd.DataFrame({
        'Suffix': [suffix_map(suff) for suff in df['suffix']],
        'Length of Name': df['VTM_NM'].str.len() / denorm_len,
        'Cost': df['Actual Cost (£)'] / denorm_cost,
        'Presentation': df['Known Pres']
    })
    return features, denorm_len, denorm_cost

def get_class_weights(labels, df):
    return {l.value: len(df) / len(df[df["Known Pres"] == l.value]) for l in Presentation if l.value in labels}

def train_ml(features, labels, df, denorm_len, denorm_cost):
    clf = svm.SVC(gamma="auto", tol=1e-3, C=1e3, kernel='poly', class_weight=get_class_weights(labels, df))
    start_t = time.time()
    clf.fit(features[::10], labels[::10])
    duration = time.time() - start_t
    print(f"Score: {100 * clf.score(features[:100], labels[:100]):.2f}% accurate with {int(duration)}s training")
    
    def predict(name, cost, silent=False):
        suffix = name[-3:]
        if isinstance(cost, str):
            cost = float(cost.replace("£", ""))
        if not silent:
            print("Predicting for", f"-{suffix}", "with", len(name), "characters, costing", f"£{cost:.2f}")
        prediction = clf.predict([[suffix_map(suffix), len(name) / denorm_len, cost / denorm_cost]])
        return Presentation(prediction[0]).name

    return clf, predict

def decision_by_suffix(suff, clf, features, denorm_len, denorm_cost):
    fig, ax = plt.subplots()
    x = features[:, 0]
    y = features[:, 1]
    xx, yy = np.meshgrid(
        np.arange(x.min(), x.max(), 0.01),
        np.arange(y.min(), y.max(), 0.01)
    )
    zz = clf.predict([
        (suffix_map(suff), x, y) for x, y in np.c_[xx.ravel(), yy.ravel()]
    ])
    zz = zz.reshape(xx.shape).astype(float) + 0.5
    
    contours = ax.contourf(
        xx * denorm_len,
        yy * denorm_cost,
        zz,
        extend="max",
        levels=[p.value for p in Presentation]
    )
    proxy = [
        plt.Rectangle((0, 0), 1, 1, fc=contour.get_facecolor()[0]) 
        for contour in contours.collections
    ]
    ax.legend(proxy, [Presentation(int(round(l))).name for l in contours.levels])
    ax.set_ylabel('Cost')
    ax.set_xlabel('Name length')
    ax.set_xlim(left=0)
    ax.set_ylim(bottom=0)
    ax.set_title('How prediction happens...')
    plt.show()

def get_frequencies_for_predictions(classifier, feat):
    freq = pd.DataFrame({"Frequency": {
        Presentation(l).name: f
        for l, f in
        zip(*np.unique(classifier.predict(feat), return_counts=True))
    }})
    return freq

def check_sample(df, predictor):
    df_sample = pd.DataFrame(df.iloc[::10])
    df_sample['Guessed Pres'] = df_sample.apply(lambda row: Presentation[predictor(row)].value, axis=1)
    vc = (df_sample['Known Pres'] == df_sample['Guessed Pres']).value_counts()
    print(f"Correct {100 * vc[True] / vc.sum():.2f}% of the time")
    
def add_known_pres(df, tablets, capsules, drops):
    def clean_presentation(row):
        presentation = row['Presentation']

        if presentation in tablets:
            return Presentation.TABLET.value
        if presentation in capsules:
            return Presentation.CAPSULE.value
        if presentation in drops:
            return Presentation.DROP.value
        return Presentation.OTHER.value
    df['Known Pres'] = df.apply(clean_presentation, axis=1)
    df = df[df['Known Pres'] != Presentation.OTHER.value]
    return df

def extract_suffices(col):
    return pd.Series(col.str.slice(-3))